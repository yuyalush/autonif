# ハンズオン手順

## ServerでCRUD

- サーバの新規作成
- サーバのメニュー変更
- サーバのコピー
- サーバの削除（後ほど）

## DMZ編 準備

- メールアドレス作成
- Nitrous作成
- ニフクラサーバ作成  
   追加：　firewallで8080もあける
- Nitrous Box作成
- SSHキーアップロード
```
  chmod 0600 sshkey_private.pem
	ssh -i sshkey_private.pem root@アドレス
  yes
  パスワード入力
```

- OSのアップデート 
```
	apt-get update
	apt-get -y upgrade
	reboot
```

## 自動作成

- Firewall修正  
   port 8080 - 8080 を追加
- ソースコードの取得
```
  git clone https://yuyalush@bitbucket.org/yuyalush/autonif.git
```

- setting.yamlを修正
```
  access_keyとsercret_keyはニフティクラウドの
    
  2行目　accsess_key
  3行目　sercret_key
  20行目　ＳＳＨキーのパスフレ
```

- ライブラリのインストール
```
	以下、Nitrousのコンソールにて

  cd autonif
	bundle
```

- 各種確認
 - キーの名前はsshkey_private.pem
 - Firewallの名前はfrontserver
 - Firewallにて8080をあけてある

- 実行
```
  ruby create_instance.rb
```

### 備考

リージョンはwest-1のみにしています。  
例：
```
west-1
27
apitest
small
```

